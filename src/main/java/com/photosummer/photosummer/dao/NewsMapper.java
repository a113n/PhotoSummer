package com.photosummer.photosummer.dao;

import com.photosummer.photosummer.dto.NewsDetailDTO;
import org.springframework.web.bind.annotation.ResponseBody;
import java.util.List;

@ResponseBody
public interface NewsMapper {

    List<NewsDetailDTO> getAllNews();

    NewsDetailDTO getNewsDetailDTOById(Integer id);

}