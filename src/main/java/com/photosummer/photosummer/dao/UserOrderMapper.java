package com.photosummer.photosummer.dao;


import com.photosummer.photosummer.dto.OrderDTO;
import com.photosummer.photosummer.entity.Order;
import com.photosummer.photosummer.entity.Photographer;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;
import java.util.Date;
import java.util.List;

@Repository
public interface UserOrderMapper {


    /***
     *查询用户下订单列表
     * @param id  用户id
     * @param statusList 可为空
     * @param bookDay    可为空
     * @return
     */
    List<Order> getUserOrderListById(@Param("id") String id, @Param("statusList") List<String> statusList, @Param("bookDay") Date bookDay);

    /**
     * 查询用户下订单数量
     * @param id
     * @param statusList
     * @param bookDay
     * @return
     */
    int getUserOrderCountById(@Param("id") String id, @Param("statusList") List<String> statusList, @Param("bookDay") Date bookDay);

    /***
     * 根据订单id查询订单详情
     * @param id
     * @return
     */
    Order getUserOrderDetail(String id);

    /***
     *查询摄影师下订单列表
     * @param id  用户id
     * @param statusList 可为空
     * @param bookDay    可为空
     * @return
     */
    List<Order>getPhotographerOrderListById(@Param("id") String id, @Param("statusList") List<String> statusList, @Param("bookDay") Date bookDay);

    /***
     * 根据订单id查询订单详情
     * @param id
     * @return
     */
    Order getPhotographerOrderDetail(String id);

    /***
     * 后台查询订单列表
     * @param userName 可为空
     * @param photographerName 可为空
     * @param statusList 可为空
     * @param bookDay 可为空
     * @param bookTime 可为空
     * @param mobile 可为空
     * @return
     */
    List<OrderDTO> getOrderListInBackground(@Param("username") String userName, @Param("photographerName") String photographerName, @Param("statusList") List<String> statusList,
                                            @Param("bookDay") Date bookDay, @Param("bookTime") String bookTime, @Param("mobile") String mobile);

    /***
     * 添加订单
     * @param order
     * @return
     */
    int addNewOrder(Order order);


    /***更新订单详情
     *
     * @param order
     * @return
     */
    int updateOrder(Order order);


    /**
     * 得到某天可预约时间段
     * @param bookDay
     * @return
     */
    List<Integer> getAvailableBookTime(Date bookDay);

    /**
     *得到符合要求的摄影师
     * @param bookDay
     * @param bookTime
     * @return
     */
    Photographer getAvailablePhotographer(@Param("bookDay") Date bookDay, @Param("bookTime") int bookTime);

    /**
     * 取消订单
     * @param orderId
     * @return
     */
    int CancelOrderByUser(@Param("id") String orderId);


}
