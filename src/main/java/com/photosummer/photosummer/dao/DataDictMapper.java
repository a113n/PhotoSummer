package com.photosummer.photosummer.dao;

import com.photosummer.photosummer.entity.DataDict;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface DataDictMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(DataDict record);

    int insertSelective(DataDict record);

    DataDict selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(DataDict record);

    int updateByPrimaryKey(DataDict record);

    List<DataDict> getByClass(@Param("firstClass")String firstClass, @Param("scnClass")String scnClass, @Param("thClass")String thClass);
}