package com.photosummer.photosummer.dao;

import com.photosummer.photosummer.entity.User;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface UserMapper {

    int insertByWeChat(@Param("id") String id, @Param("nickname") String nickname, @Param("sex") int sex);

    int deleteByPrimaryKey(String id);

    User selectByPrimaryKeyPro(String id);

    int insert(User record);

    int insertSelective(User record);

    User selectByPrimaryKey(String id);

    int updateByPrimaryKeySelective(User record);

    int updateByPrimaryKey(User record);

    int updateUser(User user);

}