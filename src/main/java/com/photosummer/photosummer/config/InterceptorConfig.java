package com.photosummer.photosummer.config;

import com.photosummer.photosummer.weixin.interceptor.WeixinLoginInterceptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * @Author: liangbin
 * @Date: 2018/3/25 14:09
 */
@Configuration
public class InterceptorConfig implements WebMvcConfigurer {

    @Autowired
    WeixinLoginInterceptor weixinLoginInterceptor;

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        //registry.addInterceptor(backManagerLoginInterceptor).addPathPatterns("/bksystem/**").excludePathPatterns("/bksystem/backsyslogin");
        registry.addInterceptor(weixinLoginInterceptor).addPathPatterns("/user/**").addPathPatterns("/order/**").addPathPatterns("/static/wx/**");

    }
}
