package com.photosummer.photosummer.controller;

import com.photosummer.photosummer.entity.DataDict;
import com.photosummer.photosummer.entity.Order;
import com.photosummer.photosummer.entity.User;
import com.photosummer.photosummer.service.DataDictService;
import com.photosummer.photosummer.service.UserOrderService;
import com.photosummer.photosummer.util.DateUtil;
import com.photosummer.photosummer.weixin.controller.Base;
import com.photosummer.photosummer.weixin.util.WeiXinFinalValue;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import javax.servlet.http.HttpSession;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

@RequestMapping("/order")
@Controller
public class OrderController {
    @Autowired
    private UserOrderService userOrderService;

    @Autowired
    private DataDictService dataDictService;

    @ApiOperation("获取用户订单列表")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType="query", name = "status", value = "订单状态", dataType = "String"),
            @ApiImplicitParam(paramType="query", name = "bookDay", value = "预约日期", dataType = "String"),
    })
    @GetMapping("/orders")
    public ModelAndView getuserOrderLiset(HttpSession session, String status, String bookDay){
        ModelAndView mv = new ModelAndView();
        List<String> statusList = null;
        Date _bookDay = null;
        try{
            if(Base.notEmpty(status)){
                statusList = Arrays.asList(status.split(","));
            }
            if(Base.notEmpty(bookDay)){
                _bookDay = new SimpleDateFormat("yyyy-MM-dd").parse(bookDay);
            }
            User user = (User)session.getAttribute(WeiXinFinalValue.WX_SESSION_USER);
            if(user==null||user.getId()==null){
                throw new Exception("用户登录状态异常");
            }
            List<Order> list = userOrderService.getUserOrderListById(user.getId(),statusList,_bookDay);
            mv.setViewName("order_list");
            mv.getModelMap().addAttribute("orderList",list);
            mv.getModelMap().addAttribute("status",200);
        }catch(Exception e){
            mv.getModelMap().addAttribute("status",-1);
        }
        return mv;
    }

    @ApiOperation("获取用户订单列表post")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType="query", name = "status", value = "订单状态", dataType = "String"),
            @ApiImplicitParam(paramType="query", name = "bookDay", value = "预约日期", dataType = "String"),
    })
    @PostMapping("/ordersPost")
    @ResponseBody
    public ModelMap getOrderLiset(HttpSession session, String status, String bookDay){
        ModelMap m = new ModelMap();
        List<String> statusList = null;
        Date _bookDay = null;
        try{
            if(Base.notEmpty(status)){
                statusList = Arrays.asList(status.split(","));
            }
            if(Base.notEmpty(bookDay)){
                _bookDay = new SimpleDateFormat("yyyy-MM-dd").parse(bookDay);
            }
            User user = (User)session.getAttribute(WeiXinFinalValue.WX_SESSION_USER);
            if(user==null||user.getId()==null){
                throw new Exception("用户登录状态异常");
            }
            List<Order> list = userOrderService.getUserOrderListById(user.getId(),statusList,_bookDay);
            m.addAttribute("orderList",list);
            m.addAttribute("status",200);
        }catch(Exception e){
            m.addAttribute("status",-1);
        }
        return m;
    }

    @ApiOperation("获取用户订单数量")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType="query", name = "status", value = "订单状态", dataType = "String"),
            @ApiImplicitParam(paramType="query", name = "bookDay", value = "预约日期", dataType = "String"),
    })
    @PostMapping("/orderCount")
    @ResponseBody
    public ModelMap getOrderCountById(HttpSession session, String status, String bookDay){
        ModelMap m = new ModelMap();
        List<String> statusList = null;
        Date _bookDay = null;
        try{
            if(Base.notEmpty(status)){
                statusList = Arrays.asList(status.split(","));
            }
            if(Base.notEmpty(bookDay)){
                _bookDay = new SimpleDateFormat("yyyy-MM-dd").parse(bookDay);
            }
            User user = (User)session.getAttribute(WeiXinFinalValue.WX_SESSION_USER);
            if(user==null||user.getId()==null){
                throw new Exception("用户登录状态异常");
            }
            int count = userOrderService.getUserOrderCountById(user.getId(),statusList,_bookDay);
            m.addAttribute("orderCount",count);
            m.addAttribute("status",200);
        }catch(Exception e){
            m.addAttribute("status",-1);
        }
        return m;
    }

    @ApiOperation("得到用户订单详情")
    @ApiImplicitParam(paramType="query", name = "id", value = "订单id", required = true, dataType = "String")
    @GetMapping("/detail")
    public ModelAndView getUserOrderDetail(String id){
        ModelAndView mv = new ModelAndView();
        try{
            Order order = userOrderService.getUserOrderDetail(id);
            mv.getModelMap().addAttribute("orderDetail",order);
            mv.setViewName("order_detail");
            mv.getModelMap().addAttribute("status",200);
        }catch(Exception e){
            e.printStackTrace();
            mv.getModelMap().addAttribute("status",-1);
        }
        return mv;
    }

    @GetMapping("/index")
    @ApiOperation(value="预约首页")
    public ModelAndView orderIndex(HttpSession session){
        ModelAndView mv = new ModelAndView();
        mv.setViewName("order_confirm");
        try{
            User user = (User)session.getAttribute(WeiXinFinalValue.WX_SESSION_USER);
            List<DataDict> list =dataDictService.getByClass("1","1","1");
            mv.getModelMap().addAttribute("departmentList",list);
            mv.getModelMap().addAttribute("name",user.getUserName());
            mv.getModelMap().addAttribute("mobile",user.getMobile());
            mv.getModelMap().addAttribute("status",200);
            return mv;
        }catch(Exception e){
            mv.getModelMap().addAttribute("status",-1);
            e.printStackTrace();
        }
        return mv;
    }
    @GetMapping("/chooseDate1")
    @ApiOperation(value="预约首页")
    public ModelAndView chooseDate1(HttpSession session){
        ModelAndView mv = new ModelAndView();
        mv.setViewName("order_date1");
        return mv;
    }
    @GetMapping("/chooseDate")
    @ApiOperation(value="预约首页")
    public ModelAndView chooseDate(HttpSession session){
        ModelAndView mv = new ModelAndView();
        mv.setViewName("order_date");
        Date date = new Date();
        try{
            mv.getModelMap().addAttribute("nowDate",new SimpleDateFormat("yyyy-MM-dd").format(date));
            date = DateUtil.addDate(date,0,1,0,0,0,0,0);
            mv.getModelMap().addAttribute("maxDate",new SimpleDateFormat("yyyy-MM-dd").format(date));
            date = DateUtil.addDate(date,0,-1,1,0,0,0,0);
            mv.getModelMap().addAttribute("tomorrow",new SimpleDateFormat("yyyy-MM-dd").format(date));
            date = DateUtil.addDate(date,0,0,1,0,0,0,0);
            mv.getModelMap().addAttribute("afterTomorrow",new SimpleDateFormat("yyyy-MM-dd").format(date));
            mv.getModelMap().addAttribute("status",200);
        }catch(Exception e){
            mv.getModelMap().addAttribute("status",-1);
            e.printStackTrace();
        }
        return mv;
    }



    @ApiOperation(value = "用户添加订单")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType="body", name = "bookDay", value = "预约日期", dataType = "String"),
            @ApiImplicitParam(paramType="body", name = "bookTime", value = "预约场次", dataType = "String"),
            @ApiImplicitParam(paramType="body", name = "mobile", value = "联系电话", dataType = "String"),
            @ApiImplicitParam(paramType="body", name = "name", value = "真实姓名", dataType = "String"),
            @ApiImplicitParam(paramType="body", name = "note", value = "备注", dataType = "String"),
            @ApiImplicitParam(paramType="body", name = "maleCount", value = "男生人数", dataType = "String"),
            @ApiImplicitParam(paramType="body", name = "femaleCount", value = "女生人数", dataType = "String")
    })
    @PostMapping("/add")
    @ResponseBody
    public ModelMap userAddOrder(
            String bookDay,//预约天
            Integer bookTime,//预约场次
            String mobile,//手机号码
            String name,//预约姓名
            String note,//备注
            Integer maleCount,//男生人数
            Integer femaleCount,//女生人数
            HttpSession session){
        ModelMap m = new ModelMap();
        try{
            if(isBlank(bookDay)&&isBlank(mobile)&&isBlank(name)&&maleCount+femaleCount<=0&&bookTime<=0){
               m.addAttribute("status",-1);
               m.addAttribute("message","插入订单失败");
                return m;
            }
            Date _bookDay = new SimpleDateFormat("yyyy-MM-dd").parse(bookDay);
            User user = (User) session.getAttribute(WeiXinFinalValue.WX_SESSION_USER);
            String orderId = new SimpleDateFormat("yyMMddhhmmss").format(System.currentTimeMillis())+(int)Math.random()*100000;
            Order order = new Order(orderId,_bookDay,bookTime,user.getId(),maleCount,femaleCount,"0",note,mobile,name);
            boolean result  = userOrderService.addUserOrder(order);
            if(result){
                m.addAttribute("status",200);
                m.addAttribute("message","添加订单成功");
                return m;
            }else{
                m.addAttribute("status",-1);
                m.addAttribute("message","插入订单失败");
            }
        }catch(Exception e){
            e.printStackTrace();
            m.addAttribute("status",-2);
            m.addAttribute("message","插入操作异常");
        }
        return m;
    }


    @ApiOperation(value = "撤销订单")
    @ApiImplicitParam(paramType="query",name="id",value="订单id",dataType = "String")
    @GetMapping("/cancelOrder")
    public String cancelOrder(String id){
        ModelAndView mv = new ModelAndView();
        try{
            int i = userOrderService.cancelOrder(id);
            if(i>0) {
                mv.getModelMap().addAttribute("status",200);
                mv.getModelMap().addAttribute("message","删除订单成功");
            }else{
                mv.getModelMap().addAttribute("status",-1);
                mv.getModelMap().addAttribute("message","删除订单失败");
            }
        }catch(Exception e){
            e.printStackTrace();
            mv.getModelMap().addAttribute("status",-2);
            mv.getModelMap().addAttribute("message","系统异常");
        }
        return "redirect:/order/orders";
    }

    public boolean isBlank(String str) {
        return str == null || str.trim().isEmpty();
    }

}
