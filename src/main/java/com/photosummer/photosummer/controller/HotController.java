package com.photosummer.photosummer.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/hot")
public class HotController {

    @GetMapping("/index")
    public String index(){
        return "redirect:/";
    }

}
