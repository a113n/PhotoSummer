package com.photosummer.photosummer.controller;

import com.photosummer.photosummer.service.NewsService;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;


@Controller
public class NewsController {

    @Autowired
    private NewsService newsService;


    @ApiOperation(value = "获取首页新闻")
    @GetMapping("/")
    public ModelAndView showIndex(){
        ModelAndView mv = new ModelAndView();
        try {
            mv.getModelMap().addAttribute("newsList",newsService.getAllNews());
            mv.setViewName("index");
            return mv;
        }catch(Exception e){
            e.printStackTrace();
        }
        return null;
    }
    @ApiOperation(value = "获取新闻详情")
    @ApiImplicitParam(paramType = "query",name="id",value="新闻id",required = true,dataType = "Integer")
    @GetMapping("/getNewsDetail")
    public ModelAndView getNewsDetailById(Integer id){
        ModelAndView mv = new ModelAndView();
        try {
            mv.setViewName("news_detail");
            mv.getModelMap().addAttribute("newsDetail",newsService.getNewsDetailDTOById(id));
            return mv;
        }catch(Exception e){
            e.printStackTrace();
        }
        return null;
    }
}
