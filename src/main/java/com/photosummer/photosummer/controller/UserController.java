package com.photosummer.photosummer.controller;

import com.photosummer.photosummer.dto.UserDTO;
import com.photosummer.photosummer.entity.User;
import com.photosummer.photosummer.service.UserOrderService;
import com.photosummer.photosummer.service.UserService;
import com.photosummer.photosummer.weixin.util.WeiXinFinalValue;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Controller
@RequestMapping("/user")
public class UserController {

    @Autowired
    private UserService userService;

    @Autowired
    private UserOrderService userOrderService;

    @ApiOperation(value = "跳转到二维码页面")
    @GetMapping("/share")
    public String showCode(){
        return "user_code";
    }

    @ApiOperation(value="获取用户首页信息")
    @GetMapping("/index")
    public ModelAndView index(HttpSession session){
        ModelAndView mv = new ModelAndView();
        User u = (User)session.getAttribute(WeiXinFinalValue.WX_SESSION_USER);
        List<String> list = new ArrayList<>();
        list.add("3");
        int finished =  userOrderService.getUserOrderCountById(u.getId(),list,null);
        list.clear();
        list.add("0");
        list.add("1");
        list.add("2");
        list.add("3");
        int totalOrder = userOrderService.getUserOrderCountById(u.getId(),list,null);
        UserDTO user = new UserDTO(u);
        user.setFinishedOrder(finished);
        user.setTotalOrder(totalOrder);
        mv.getModelMap().addAttribute("user",user);
        mv.setViewName("user_index");
        return mv;
    }
    @ApiOperation(value="获取用户详细信息")
    @GetMapping("/info")
    public ModelAndView userDetail(HttpSession session){
        ModelAndView mv = new ModelAndView();
        User u = (User)session.getAttribute(WeiXinFinalValue.WX_SESSION_USER);
        UserDTO user = new UserDTO(u);
        mv.getModelMap().addAttribute("user",user);
        mv.setViewName("user_info");
        return mv;
    }

    @ApiOperation(value="修改用户信息")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType ="body",name="userName",value="用户姓名",required = true,dataType = "String"),
            @ApiImplicitParam(paramType ="body",name="mobile",value="用户电话",required = true,dataType = "String"),
            @ApiImplicitParam(paramType ="body",name="department",value="用户学院",required = true,dataType = "String")
    })
    @PostMapping("/update")
    @ResponseBody
    public ModelMap updateUser(User user,HttpSession session){
        ModelMap m = new ModelMap();
        try {
            User u = (User)session.getAttribute(WeiXinFinalValue.WX_SESSION_USER);
            user.setId(u.getId());
            int i = userService.updateByPrimaryKeySelective(user);
            if (i > 0) {
                m.addAttribute("status", 200);
                m.addAttribute("message", "修改用户信息成功");
            } else {
                m.addAttribute("status", -1);
                m.addAttribute("message", "修改用户信息失败");
            }
        }catch(Exception e){
            m.addAttribute("status",-2);
            m.addAttribute("message","系统异常");
        }
        return m;

    }


}
