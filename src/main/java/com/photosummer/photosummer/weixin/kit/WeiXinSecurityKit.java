package com.photosummer.photosummer.weixin.kit;

import java.security.MessageDigest;

public class WeiXinSecurityKit {
	public static String shal(String str)throws Exception{
		StringBuffer buffer=null;
	try {
		 buffer=new StringBuffer();
			MessageDigest messageDigest=MessageDigest.getInstance("sha1");
			messageDigest.update(str.getBytes());
		for(byte bs:messageDigest.digest()){
			buffer.append(String.format("%02x",bs));
		}
		return buffer.toString();
	} catch (Exception e) {
		e.printStackTrace();
		return null;
	}
}
}
