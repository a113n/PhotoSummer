package com.photosummer.photosummer.weixin.kit;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.photosummer.photosummer.weixin.controller.Base;
import com.photosummer.photosummer.weixin.util.JsonUtil;

import java.io.IOException;

/**
 * 微信返回码校验工具类
 * @author 程序媛
 * QQ群 群1： 494705674 群2:605806884
 */
public class WeiXinCheckKit {
	/**
	 * 检查请求是否成功
	 * @return
	 */
	public static boolean checkRequestSucc(String content) {
		try {//取出状态信息进行json数据解析 从而获取请求状态
			if(Base.notEmpty(content)){
				JsonNode jn = JsonUtil.getMapper().readTree(content);
				if(!jn.has("errcode")) return true;
				if(jn.get("errcode").asInt()==0) return true;
			}
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return false;
	}
	/**
	 * 获取状态码
	 * @param content  公众号平台请求接口所返回的json数据
	 * @return
	 */
	public static int getRequestCode(String content) {
		try {
			JsonNode jn = JsonUtil.getMapper().readTree(content);
			if(jn.has("errcode")) return jn.get("errcode").asInt();
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return -1;
	}
	/**
	 * 获取状态信息
	 * @param content  公众号平台请求接口所返回的json数据
	 * @return
	 */
	public static String getRequestMsg(String content) {
		try {
			JsonNode jn = JsonUtil.getMapper().readTree(content);
			if(jn.has("errcode")) return jn.get("errmsg").asText();
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}
	
}
