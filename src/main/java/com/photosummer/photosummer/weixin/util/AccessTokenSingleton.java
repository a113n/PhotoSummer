package com.photosummer.photosummer.weixin.util;


import com.photosummer.photosummer.weixin.controller.Base;
import com.photosummer.photosummer.weixin.dto.AccessTokenDTO;

/**
 * 微信配置  为单实例Bean  主要用于存储 AccessToken  解决了不需要存储在缓存或者数据库内 或者写在文件中
 * @author lyl
 */
public class AccessTokenSingleton {
 private static AccessTokenSingleton cfg;
	private AccessTokenSingleton(){}
	private AccessTokenDTO accessTokenDTO;
	public static AccessTokenSingleton getInstance(){
		if(Base.empty(cfg)) cfg=new AccessTokenSingleton();
		return cfg;
	}
	public AccessTokenDTO getAccessToken() {
		return accessTokenDTO;
	}
	public void setAccessToken(AccessTokenDTO accessTokenDTO) {
		this.accessTokenDTO = accessTokenDTO;
	}
}
