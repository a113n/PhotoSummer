package com.photosummer.photosummer.weixin.util;

/**
  *微信常量类 
 * @author lyl
 * 
 */
public class WeiXinFinalValue {
	
public static final String APPID="wxac28ccd380d658a6";

public static final String APPSECRET="ac79808d1020587c1335e17ef3a75f09";

public static final String TOKEN="StaySummer2018";

public static final String WX_SESSION_USER="cbF9bUdUHwNT7scybOoq";
/**
 * 网页授权方式 SCOPE为：snsapi_base方式
 */
public static final String SNSAPI_BASE ="snsapi_base";
/**
 * 网页授权方式 SCOPE为：snsapi_userinfo方式
 */
public static final String SNSAPI_INFO="snsapi_userinfo";
/**
 * 微信接口调用的唯一票据  有效时间 7200秒
 * access_token是公众号的全局唯一票据，公众号调用各接口时都需使用access_token 开发者需要进行妥善保存。access_token的存储至少要保留512个字符空间。access_token的有效期目前为2个小时，需定时刷新，重复获取将导致上次获取的access_token失效。 
 */
public static final String GET_ACCESSTOKEN="https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid=APPID&secret=APPSECRET";
/**
 * 微信网页OAuth2.0鉴权
 */
public static final String WX_AUTHORIZE="https://open.weixin.qq.com/connect/oauth2/authorize?appid=APPID&redirect_uri=REDIRECT_URI&response_type=code&scope=SCOPE&state=STATE#wechat_redirect";
/**
 * 获取微信网页授权所用的AccessToken
 */
public static final String WX_AUTHORIZE_ACCESSTOKEN="https://api.weixin.qq.com/sns/oauth2/access_token?appid=APPID&secret=SECRET&code=CODE&grant_type=authorization_code";
/**
 * 根据网页授权的AccessToken以及openId  来获取用户信息
 */
public static final String WX_AUTHORIZE_INFO="https://api.weixin.qq.com/sns/userinfo?access_token=ACCESS_TOKEN&openid=OPENID&lang=zh_CN";


}
