package com.photosummer.photosummer.weixin.service;


import com.alibaba.fastjson.JSONObject;

import java.io.UnsupportedEncodingException;

/**
 * Created by VS on 2018/4/11.
 */
public interface AuthService {
    public abstract String getAccessToken(String code) throws Exception;
    public abstract String getOpenId(String accessToken);
    public abstract String refreshToken(String code);
    public abstract String getAuthorizationUrl() throws UnsupportedEncodingException;
    public abstract JSONObject getUserInfo(String accessToken, String openId) throws Exception;
}
