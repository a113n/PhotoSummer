package com.photosummer.photosummer.weixin.service;


import com.alibaba.fastjson.JSONObject;

/**
 * Created by VS on 2018/4/11.
 */
public interface WeChatAuthService extends AuthService{
    public JSONObject getUserInfo(String accessToken, String openId) throws Exception;
}

