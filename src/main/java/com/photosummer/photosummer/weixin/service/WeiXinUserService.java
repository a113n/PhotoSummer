package com.photosummer.photosummer.weixin.service;


import com.photosummer.photosummer.weixin.dto.WeiXinAuthorizeDTO;
import com.photosummer.photosummer.weixin.dto.WeiXinUserInfoDTO;
import com.photosummer.photosummer.weixin.exception.WxErrorException;

/**
 * 微信用户接口
 * @author
 */
public interface WeiXinUserService {
	/**
	 * 根据code获取拉取用户信息的AccessToken
	 * @param code
	 * @return
	 * @throws Exception
	 */
	public WeiXinAuthorizeDTO getAuthorizeAccessTokenService(String code) throws WxErrorException;
	
	/**
	 * 根据accessToken和openId拉取用户信息
	 * @return
	 * @throws Exception
	 */
	public WeiXinUserInfoDTO getAuthorizeUserInfo(WeiXinAuthorizeDTO weiXinAuthorizeDTO)throws WxErrorException;
}
