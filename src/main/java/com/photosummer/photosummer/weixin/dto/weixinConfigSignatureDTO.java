package com.photosummer.photosummer.weixin.dto;
import java.io.Serializable;

/**
 * 微信配置实体类
 * @author lyl
 *
 */
public class weixinConfigSignatureDTO implements Serializable {
private static final long serialVersionUID = 1L;
private String signature;
private String timestamp;
private String nonce;
private String echostr;
public String getSignature() {
	return signature;
}
public void setSignature(String signature) {
	this.signature = signature;
}
public String getTimestamp() {
	return timestamp;
}
public void setTimestamp(String timestamp) {
	this.timestamp = timestamp;
}
public String getNonce() {
	return nonce;
}
public void setNonce(String nonce) {
	this.nonce = nonce;
}
public String getEchostr() {
	return echostr;
}
public void setEchostr(String echostr) {
	this.echostr = echostr;
}

}
