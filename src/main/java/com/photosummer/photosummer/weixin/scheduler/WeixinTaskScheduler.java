package com.photosummer.photosummer.weixin.scheduler;

import com.alibaba.fastjson.JSONObject;
import com.photosummer.photosummer.weixin.dto.AccessTokenDTO;
import com.photosummer.photosummer.weixin.exception.WxErrorException;
import com.photosummer.photosummer.weixin.kit.WeiXinCheckKit;
import com.photosummer.photosummer.weixin.service.WeixinBaseService;
import com.photosummer.photosummer.weixin.util.AccessTokenSingleton;
import com.photosummer.photosummer.weixin.util.WeiXinFinalValue;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

/**
 * Created by VS on 2018/4/10.
 */
@Component
public class WeixinTaskScheduler {
    @Autowired
    private WeixinBaseService weixinBaseService;
    /**
    * 定时获取access_token
    * */
    @Scheduled(fixedDelay =6000000)
    public void runTask(){
        String resContent;
        try {
            String url= WeiXinFinalValue.GET_ACCESSTOKEN;
            url=url.replace("APPID", WeiXinFinalValue.APPID).replace("APPSECRET", WeiXinFinalValue.APPSECRET);
            resContent=weixinBaseService.get(url, null);
            if(WeiXinCheckKit.checkRequestSucc(resContent)){
                //AccessTokenDTO accessTokenDTO=(AccessTokenDTO) JsonUtil.getInstance().json2obj(resContent, AccessTokenDTO.class);
                AccessTokenDTO accessTokenDTO= JSONObject.parseObject(resContent,AccessTokenDTO.class);
                AccessTokenSingleton.getInstance().setAccessToken(accessTokenDTO);
                System.out.println("【获取到了AccessToken】==========>>"+ AccessTokenSingleton.getInstance().getAccessToken().getAccess_token());
            }else{
                System.out.println("【获取AccessToken失败】"+resContent);
            }
        } catch (WxErrorException e) {
            e.printStackTrace();
        }
    }
}
