package com.photosummer.photosummer.weixin.excutor;

import com.photosummer.photosummer.weixin.controller.Base;
import com.photosummer.photosummer.weixin.exception.WxErrorException;
import com.photosummer.photosummer.weixin.http.Utf8ResponseHandler;
import org.apache.http.Consts;
import org.apache.http.HttpHost;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;

import java.io.IOException;

/**
 * 简单的POST  Http 请求执行器
 * @author 程序媛
 * QQ群 群1： 494705674 群2:605806884
 */
public class SimplePostRequestExecutor implements RequestExecutor<String, String> {
  @Override
  public String execute(CloseableHttpClient httpclient, HttpHost httpProxy, String url, String postEntity) throws WxErrorException, IOException {
	    HttpPost httpPost = new HttpPost(url);
	    if (httpProxy != null) {
	      RequestConfig config = RequestConfig.custom().setProxy(httpProxy).build();
	      httpPost.setConfig(config);
	    }
	   if(Base.notEmpty(postEntity)){
	      StringEntity entity = new StringEntity(postEntity, Consts.UTF_8);
	      httpPost.setEntity(entity);
	      try (CloseableHttpResponse response = httpclient.execute(httpPost)) {
	    	   return Utf8ResponseHandler.INSTANCE.handleResponse(response);
	      } finally {
	    	  httpPost.releaseConnection();
	      }
	    }
   	throw new RuntimeException("请求数据不能为空!");
  }

}
