package com.photosummer.photosummer.weixin.excutor;
import com.photosummer.photosummer.weixin.exception.WxErrorException;
import com.photosummer.photosummer.weixin.http.Utf8ResponseHandler;
import org.apache.http.HttpHost;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;

import java.io.IOException;

/**
 * 简单的http GET请求执行器
 * @author 程序媛
 *QQ群 群1： 494705674 群2:605806884
 */
public class SimpleGetRequestExecutor implements RequestExecutor<String, String> {
  @Override
  public String execute(CloseableHttpClient httpclient, HttpHost httpProxy, String url, String queryParam) throws WxErrorException, IOException {
    HttpGet httpGet = new HttpGet(url);
    if (httpProxy != null) {
      RequestConfig config = RequestConfig.custom().setProxy(httpProxy).build();
      httpGet.setConfig(config);
    }
    try (CloseableHttpResponse response = httpclient.execute(httpGet)) {
    	return Utf8ResponseHandler.INSTANCE.handleResponse(response);
    } finally {
      httpGet.releaseConnection();
    }
  }
}
