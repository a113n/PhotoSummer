package com.photosummer.photosummer.entity;

import java.util.Date;

public class News {
    public News(Integer id, Integer imageid, String userid, String title, String content, String image, String nickname, String avatar) {
        this.id = id;
        this.imageid = imageid;
        this.userid = userid;
        this.title = title;
        this.content = content;
        this.meta = meta;
        this.image = image;
        this.nickname = nickname;
        this.avatar = avatar;
    }

    private Integer id;

    private Integer imageid;

    private String userid;

    private String title;

    private String content;

    private Date meta;

    private String image;

    private String nickname;

    private String avatar;

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getImageid() {
        return imageid;
    }

    public void setImageid(Integer imageid) {
        this.imageid = imageid;
    }

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid == null ? null : userid.trim();
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title == null ? null : title.trim();
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content == null ? null : content.trim();
    }

    public Date getMeta() {
        return meta;
    }

    public void setMeta(Date meta) {
        this.meta = meta;
    }

}