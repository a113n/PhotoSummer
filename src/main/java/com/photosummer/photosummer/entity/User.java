package com.photosummer.photosummer.entity;

public class User {
    private String id;

    private String userName;

    private String avatar;

    private String college;

    private String department;

    private String major;

    private String batch;

    private Integer maleNum;

    private Integer femaleNum;

    private Integer ifStudent;

    private Integer ifGroup;

    private String mobile;

    private String nickname;

    private int sex;

    private String extra_message;

    public String getExtra_message() {
        return extra_message;
    }

    public void setExtra_message(String extra_message) {
        this.extra_message = extra_message;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public int getSex() {
        return sex;
    }

    public void setSex(int sex) {
        this.sex = sex;
    }

    public String getDepartment() {
        return department;
    }

    public void setDepartment(String department) {
        this.department = department;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id == null ? null : id.trim();
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName == null ? null : userName.trim();
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar == null ? null : avatar.trim();
    }

    public String getCollege() {
        return college;
    }

    public void setCollege(String college) {
        this.college = college == null ? null : college.trim();
    }

    public String getMajor() {
        return major;
    }

    public void setMajor(String major) {
        this.major = major == null ? null : major.trim();
    }

    public String getBatch() {
        return batch;
    }

    public void setBatch(String batch) {
        this.batch = batch == null ? null : batch.trim();
    }

    public Integer getMaleNum() {
        return maleNum;
    }

    public void setMaleNum(Integer maleNum) {
        this.maleNum = maleNum;
    }

    public Integer getFemaleNum() {
        return femaleNum;
    }

    public void setFemaleNum(Integer femaleNum) {
        this.femaleNum = femaleNum;
    }

    public Integer getIfStudent() {
        return ifStudent;
    }

    public void setIfStudent(Integer ifStudent) {
        this.ifStudent = ifStudent;
    }

    public Integer getIfGroup() {
        return ifGroup;
    }

    public void setIfGroup(Integer ifGroup) {
        this.ifGroup = ifGroup;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    @Override
    public boolean equals(Object that) {
        if (this == that) {
            return true;
        }
        if (that == null) {
            return false;
        }
        if (getClass() != that.getClass()) {
            return false;
        }
        User other = (User) that;
        return (this.getId() == null ? other.getId() == null : this.getId().equals(other.getId()))
            && (this.getUserName() == null ? other.getUserName() == null : this.getUserName().equals(other.getUserName()))
            && (this.getAvatar() == null ? other.getAvatar() == null : this.getAvatar().equals(other.getAvatar()))
            && (this.getCollege() == null ? other.getCollege() == null : this.getCollege().equals(other.getCollege()))
            && (this.getMajor() == null ? other.getMajor() == null : this.getMajor().equals(other.getMajor()))
            && (this.getBatch() == null ? other.getBatch() == null : this.getBatch().equals(other.getBatch()))
            && (this.getMaleNum() == null ? other.getMaleNum() == null : this.getMaleNum().equals(other.getMaleNum()))
            && (this.getFemaleNum() == null ? other.getFemaleNum() == null : this.getFemaleNum().equals(other.getFemaleNum()))
            && (this.getIfStudent() == null ? other.getIfStudent() == null : this.getIfStudent().equals(other.getIfStudent()))
            && (this.getIfGroup() == null ? other.getIfGroup() == null : this.getIfGroup().equals(other.getIfGroup()))
            && (this.getMobile() == null ? other.getMobile() == null : this.getMobile().equals(other.getMobile()));
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((getId() == null) ? 0 : getId().hashCode());
        result = prime * result + ((getUserName() == null) ? 0 : getUserName().hashCode());
        result = prime * result + ((getAvatar() == null) ? 0 : getAvatar().hashCode());
        result = prime * result + ((getCollege() == null) ? 0 : getCollege().hashCode());
        result = prime * result + ((getMajor() == null) ? 0 : getMajor().hashCode());
        result = prime * result + ((getBatch() == null) ? 0 : getBatch().hashCode());
        result = prime * result + ((getMaleNum() == null) ? 0 : getMaleNum().hashCode());
        result = prime * result + ((getFemaleNum() == null) ? 0 : getFemaleNum().hashCode());
        result = prime * result + ((getIfStudent() == null) ? 0 : getIfStudent().hashCode());
        result = prime * result + ((getIfGroup() == null) ? 0 : getIfGroup().hashCode());
        result = prime * result + ((getMobile() == null) ? 0 : getMobile().hashCode());
        return result;
    }

    @Override
    public String toString() {
        return "User{" +
                "id='" + id + '\'' +
                ", userName='" + userName + '\'' +
                ", nickname='" + nickname +'\''+
                ", avatar='" + avatar + '\'' +
                ", sex='" + sex+'\''+
                ", college='" + college + '\'' +
                ", department='" + department + '\'' +
                ", major='" + major + '\'' +
                ", batch='" + batch + '\'' +
                ", maleNum=" + maleNum +
                ", femaleNum=" + femaleNum +
                ", ifStudent=" + ifStudent +
                ", ifGroup=" + ifGroup +
                ", mobile=" + mobile +
                '}';
    }
}