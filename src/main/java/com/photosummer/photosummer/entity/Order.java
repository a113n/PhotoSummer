package com.photosummer.photosummer.entity;

import org.springframework.jdbc.datasource.SimpleDriverDataSource;

import java.io.Serializable;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Order implements Serializable{
    private String id;

    private String photographerId;

    private String userid;

    private User user;

    private Date createTime;

    private Date endTime;

    private BigDecimal price;

    private String status;

    private Date bookDay;

    private String bookDayS;

    private int bookTime;

    private Date finishTime;

    private String extraMessage;

    private int maleNum;

    private int femaleNum;

    private String mobile;

    private String userName;

    private String department;

    private Integer totalCount;

    public Integer getTotalCount() {
        return totalCount;
    }

    public void setTotalCount(Integer totalCount) {
        this.totalCount = totalCount;
    }

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public Order(String id){
        this.id = id;
    }
    public Order(String id, Date bookDay, int bookTime, String userid, int maleNum, int femaleNum, String status, String extraMessage,String mobile,String name){
        this.id = id;
        this.bookDay = bookDay;
        this.bookTime = bookTime;
        this.userid = userid;
        this.maleNum = maleNum;
        this.femaleNum = femaleNum;
        this.status = status;
        this.extraMessage = extraMessage;
        this.mobile = mobile;
        this.userName = name;
    }
    public Order(){

    }

    public String getDepartment() {
        return department;
    }

    public void setDepartment(String department) {
        this.department = department;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getExtraMessage() {
        return extraMessage;
    }

    public void setExtraMessage(String extraMessage) {
        this.extraMessage = extraMessage;
    }

    public Date getFinishTime() {
        return finishTime;
    }

    public void setFinishTime(Date finishTime) {
        this.finishTime = finishTime;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id == null ? null : id.trim();
    }

    public String getPhotographerId() {
        return photographerId;
    }

    public void setPhotographerId(String photographerId) {
        this.photographerId = photographerId;
    }
    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getEndTime() {
        return endTime;
    }

    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Date getBookDay() {
        return bookDay;
    }

    public void setBookDay(Date bookDay) {
        this.bookDay = bookDay;
        this.bookDayS = new SimpleDateFormat("yyyy-MM-dd").format(bookDay);
    }

    public int getBookTime() {
        return bookTime;
    }

    public void setBookTime(int bookTime) {
        this.bookTime = bookTime;
    }

    public int getMaleNum() {
        return maleNum;
    }

    public void setMaleNum(int maleNum) {
        this.maleNum = maleNum;
    }

    public int getFemaleNum() {
        return femaleNum;
    }

    public void setFemaleNum(int femaleNum) {
        this.femaleNum = femaleNum;
    }

    public String getBookDayS() {
        return bookDayS;
    }

    public void setBookDayS(String bookDayS) {
        this.bookDayS = bookDayS;
    }
}