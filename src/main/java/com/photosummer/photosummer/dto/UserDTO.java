package com.photosummer.photosummer.dto;

import com.photosummer.photosummer.entity.User;

public class UserDTO {
    private String nickname;
    private String avatar;
    private String mobile;
    private String sex;
    private String name;
    private String department;
    private int totalOrder;
    private int finishedOrder;



    public UserDTO(){}

    public UserDTO(User user){
        this.avatar = user.getAvatar();
        this.mobile = user.getMobile();
        this.name = user.getUserName();
        this.nickname = user.getNickname();
        this.sex=(user.getSex()==0?"未知":(user.getSex()==1?"男":"女"));
        this.department = user.getDepartment();
    }
    public UserDTO(String nickname, String avatar, String mobile, String sex, String name, int totalOrder, int finishedOrder) {
        this.nickname = nickname;
        this.avatar = avatar;
        this.mobile = mobile;
        this.sex = sex;
        this.name = name;
        this.totalOrder = totalOrder;
        this.finishedOrder = finishedOrder;
    }

    public String getDepartment() {
        return department;
    }

    public void setDepartment(String department) {
        this.department = department;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(int sex) {
        this.sex=(sex==0?"未知":(sex==1?"男":"女"));
    }

    public int getTotalOrder() {
        return totalOrder;
    }

    public void setTotalOrder(int totalOrder) {
        this.totalOrder = totalOrder;
    }

    public int getFinishedOrder() {
        return finishedOrder;
    }

    public void setFinishedOrder(int finishedOrder) {
        this.finishedOrder = finishedOrder;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
