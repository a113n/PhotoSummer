package com.photosummer.photosummer;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@MapperScan("com.photosummer.photosummer.dao")
@SpringBootApplication
public class PhotosummerApplication {

	public static void main(String[] args) {
		SpringApplication.run(PhotosummerApplication.class, args);
	}
}
