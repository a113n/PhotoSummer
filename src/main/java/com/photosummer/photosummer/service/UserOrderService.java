package com.photosummer.photosummer.service;


import com.photosummer.photosummer.dto.OrderDTO;
import com.photosummer.photosummer.entity.Order;
import java.util.Date;
import java.util.List;

public interface UserOrderService {


    /***
     *查询用户下订单列表
     * @param id  用户id
     * @param statusList 可为空
     * @param bookDay    可为空
     * @return
     */
    List<Order> getUserOrderListById(String id, List<String> statusList, Date bookDay);

    /**
     * 查询用户下订单数量
     * @param id
     * @param statusList
     * @param bookDay
     * @return
     */
    int getUserOrderCountById(String id, List<String> statusList, Date bookDay);

    /***
     * 根据订单id查询订单详情
     * @param id
     * @return
     */
    Order getUserOrderDetail(String id);

    /***
     *查询摄影师下订单列表
     * @param id  用户id
     * @param statusList 可为空
     * @param bookDay    可为空
     * @return
     */
    List<Order>getPhotographerOrderListById(String id, List<String> statusList, Date bookDay);

    /***
     * 根据订单id查询订单详情
     * @param id
     * @return
     */
    Order getPhotographerOrderDetail(String id);

    /***
     * 后台查询订单列表
     * @param userName 可为空
     * @param photographerName 可为空
     * @param statusList 可为空
     * @param bookDay 可为空
     * @param bookTime 可为空
     * @param mobile 可为空
     * @return
     */
    List<OrderDTO> getOrderListInBackground(String userName, String photographerName, List<String> statusList,
                                            Date bookDay, String bookTime, String mobile);

    /***
     * 添加订单
     * @param order
     * @return
     */
    int addNewOrder(Order order);


    /***更新订单详情
     *
     * @param order
     * @return
     */
    int updateOrder(Order order);


    /***
     * 得到某天可预约时间段
     * @param bookDay
     * @return
     */
    List<Integer> getAvailableBookTime(Date bookDay);

    /**
     * 撤销订单
     * @Param id
     * @return
     */

    int cancelOrder(String id);

    /**
     * 插入新订单
     * @param order
     * @return
     */
     boolean addUserOrder(Order order);
}
