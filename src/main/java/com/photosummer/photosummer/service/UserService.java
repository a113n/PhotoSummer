package com.photosummer.photosummer.service;


import com.photosummer.photosummer.entity.User;

public interface UserService {
    int updateUser(User user);
    int insertSelective(User record);
    User selectByPrimaryKeyPro(String id);
    public int updateByPrimaryKeySelective(User user);
}
