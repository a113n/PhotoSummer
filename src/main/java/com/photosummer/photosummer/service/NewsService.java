package com.photosummer.photosummer.service;

import com.photosummer.photosummer.dto.NewsDetailDTO;
import java.util.List;

public interface NewsService {
    List<NewsDetailDTO> getAllNews();

    NewsDetailDTO getNewsDetailDTOById(Integer id);
}
