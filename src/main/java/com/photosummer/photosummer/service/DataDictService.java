package com.photosummer.photosummer.service;

import com.photosummer.photosummer.entity.DataDict;

import java.util.List;

public interface DataDictService {
    List<DataDict> getByClass(String firstClass,String scnClass,String thClass);

}
