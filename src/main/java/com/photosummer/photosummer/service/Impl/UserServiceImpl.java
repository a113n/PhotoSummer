package com.photosummer.photosummer.service.Impl;

import com.photosummer.photosummer.dao.UserMapper;
import com.photosummer.photosummer.entity.User;
import com.photosummer.photosummer.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserMapper userMapper;

    @Override
    public int updateUser(User user) {
        return userMapper.updateUser(user);
    }

    @Override
    public int insertSelective(User record){
        return userMapper.insertSelective(record);
    }

    @Override
    public User selectByPrimaryKeyPro(String id){
        return userMapper.selectByPrimaryKeyPro(id);
    }

    @Override
    public int updateByPrimaryKeySelective(User user){return userMapper.updateByPrimaryKeySelective(user);}
}
