package com.photosummer.photosummer.service.Impl;


import com.photosummer.photosummer.dao.NewsMapper;
import com.photosummer.photosummer.dto.NewsDetailDTO;
import com.photosummer.photosummer.service.NewsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.List;

@Service
public class NewsServiceImpl implements NewsService {

    @Autowired
    private NewsMapper newsMapper;

    @Override
    public List<NewsDetailDTO> getAllNews(){
        List<NewsDetailDTO> list = newsMapper.getAllNews();
        return list;
    }

    @Override
    public NewsDetailDTO getNewsDetailDTOById(Integer id) {
        NewsDetailDTO n = newsMapper.getNewsDetailDTOById(id);
        return n;
    }
}
