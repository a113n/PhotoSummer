package com.photosummer.photosummer.service.Impl;

import com.photosummer.photosummer.dao.DataDictMapper;
import com.photosummer.photosummer.entity.DataDict;
import com.photosummer.photosummer.service.DataDictService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class DataDictServiceImpl implements DataDictService {

    @Autowired
    private DataDictMapper dataDictMapper;
    @Override
    public List<DataDict> getByClass(String firstClass, String scnClass, String thClass) {
        return dataDictMapper.getByClass(firstClass,scnClass,thClass);
    }
}
