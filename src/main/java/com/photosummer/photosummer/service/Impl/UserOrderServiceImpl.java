package com.photosummer.photosummer.service.Impl;

import com.photosummer.photosummer.dao.UserOrderMapper;
import com.photosummer.photosummer.dto.OrderDTO;
import com.photosummer.photosummer.entity.Order;
import com.photosummer.photosummer.entity.User;
import com.photosummer.photosummer.service.UserOrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

@Service
public class UserOrderServiceImpl implements UserOrderService {
    @Autowired
    private UserOrderMapper userMapper;

    @Override
    public List<Order> getUserOrderListById(String id, List<String> statusList, Date bookDay) {
        return userMapper.getUserOrderListById(id,statusList,bookDay);
    }

    @Override
    public int getUserOrderCountById(String id, List<String> statusList, Date bookDay) {
        return userMapper.getUserOrderCountById(id,statusList,bookDay);
    }

    @Override
    public Order getUserOrderDetail(String id) {
        return userMapper.getUserOrderDetail(id);
    }

    @Override
    public List<Order> getPhotographerOrderListById(String id, List<String> statusList, Date bookDay) {
        return userMapper.getPhotographerOrderListById(id,statusList,bookDay);
    }

    @Override
    public Order getPhotographerOrderDetail(String id) {
        return userMapper.getPhotographerOrderDetail(id);
    }

    @Override
    public List<OrderDTO> getOrderListInBackground(String userName, String photographerName, List<String> statusList, Date bookDay, String bookTime, String mobile) {
        return userMapper.getOrderListInBackground(userName,photographerName,statusList,bookDay,bookTime,mobile);
    }

    @Override
    public int addNewOrder(Order order) {
        return userMapper.addNewOrder(order);
    }

    @Override
    public int updateOrder(Order order) {
        return userMapper.updateOrder(order);
    }

    @Override
    public List<Integer> getAvailableBookTime(Date bookDay) {
        return userMapper.getAvailableBookTime(bookDay);
    }

    @Override
    public int cancelOrder(String id) {
        return userMapper.CancelOrderByUser(id);
    }

    @Transactional
    @Override
    public boolean addUserOrder(Order order) {
        order.setPrice(new BigDecimal(30*(order.getMaleNum()+order.getFemaleNum())));
        int i = addNewOrder(order);
        if(i>0) return true;
        return false;
    }




}
