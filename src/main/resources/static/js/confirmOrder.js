/**
 * Created by fusongke on 2014/10/8.
 */

function __$(id){
    return document.getElementById(id);
}

function InitAjax()
{
    var ajax=false;
    try {
        ajax = new ActiveXObject("Msxml2.XMLHTTP");
    } catch (e) {
        try {
            ajax = new ActiveXObject("Microsoft.XMLHTTP");
        } catch (E) {
            ajax = false;
        }
    }
    if (!ajax && typeof XMLHttpRequest!='undefined') {
        ajax = new XMLHttpRequest();
    }
    return ajax;
}

function isWeixin(){
    var ua = navigator.userAgent.toLowerCase();
    if(ua.match(/MicroMessenger/i)=="micromessenger") {
        return true;
    } else {
        return false;
    }
}

$(document).ready(function(){
    if(!isWeixin()){
        $("#gopay1").hide();
//        document.getElementById("gopay1").style.display = "none";
    }
});


var errInfo = "春节放假期间哦，暂不接单，请女神见谅！";
function forbidTime(year,month,day){
    if(year == 2015 && month == 2){
        if(day == 18 || day == 19 || day == 20 || day == 21 || day == 22){
            return true;
        }
    }
    return false;
}


/***
 * 下单规则限制
 * 下单时间：22：00以后到次日8：00，只能最早下次日10：00的订单。
 * @param dayIndex    : 0：当天 1：明天 2：后天
 * @param selectTime  ：选择的小时
 */
function checkOrderTime(dayIndex,selectTime){
    var date = new Date();
    var sHour = date.getHours();
    var hour = parseInt(sHour);
    var selectTime = parseInt(selectTime);
    if(hour >=22 && hour <=24){
        if(dayIndex == 1){//明天
            if(selectTime >=8 && selectTime <10){
                alert("22:00以后到次日8:00，只能最早下次日10:00之后的订单");
                return false;
            }
        }
    }else if(hour >=0 && hour <8) {
        if(dayIndex == 0){//今天
            if(selectTime >=8 && selectTime <10){
                alert("22:00以后到次日8:00，只能最早下次日10:00之后的订单");
                return false;
            }
        }
    }
    return true;
}

/***
 * 页面访问统计
 * @param url
 * @param pageName
 */
function pageStatistic(url,pageName,viewPageUrl){
    var param ="pageName="+pageName+"&viewpageurl="+viewPageUrl+"&t="+new Date().getTime();
    var ajax=InitAjax();
    ajax.open("POST", url, true);
    ajax.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
    ajax.onreadystatechange = function() {
        //如果执行是状态正常，那么就把返回的内容赋值给上面指定的层
        if (ajax.readyState == 4 && ajax.status == 200) {
            var data=JSON.parse(ajax.responseText);
            if(data.status == 1){
            }
        }
    }
    //发送参数
    ajax.send(param);
}

/**
 * 自定义事件每月天数变化
 */
function autoDays(){

    $("#month").change(function(){
        var base = "<option>1日</option>" +
            "<option>2日</option>" +
            "<option>3日</option>" +
            "<option>4日</option>"+
            "<option>5日</option>"+
            "<option>6日</option>"+
            "<option>7日</option>"+
            "<option>8日</option>"+
            "<option>9日</option>"+
            "<option>10日</option>"+
            "<option>11日</option>"+
            "<option>12日</option>"+
            "<option>13日</option>"+
            "<option>14日</option>"+
            "<option>15日</option>"+
            "<option>16日</option>"+
            "<option>17日</option>"+
            "<option>18日</option>"+
            "<option>19日</option>"+
            "<option>20日</option>"+
            "<option>21日</option>"+
            "<option>22日</option>"+
            "<option>23日</option>"+
            "<option>24日</option>"+
            "<option>25日</option>"+
            "<option>26日</option>"+
            "<option>27日</option>"+
            "<option>28日</option>";
        var day = $("#day");
        var strMont = $(this).val();
        if(strMont != "2月"){
            if(strMont =="1月"|strMont =="3月"||strMont =="5月"||strMont =="7月"||strMont =="8月"||strMont =="10月"||strMont =="12月"){
                base = base + "<option>29日</option><option>30日</option><option>31日</option>";
            } else {
                base = base +  "<option>29日</option><option>30日</option>";
            }
        } else {
            // 判断是否为闰年
            var strYear = $("#year").val();
            var iYear = parseInt(strYear);
            if(((iYear%4==0)&&(iYear%100!=0))||(iYear%400==0)) {
                base = base + "<option>29日</option>";
            }
        }
        $("#day").html(base);
    });

    $("#year").change(function(){
        var base = "<option>1日</option>" +
            "<option>2日</option>" +
            "<option>3日</option>" +
            "<option>4日</option>"+
            "<option>5日</option>"+
            "<option>6日</option>"+
            "<option>7日</option>"+
            "<option>8日</option>"+
            "<option>9日</option>"+
            "<option>10日</option>"+
            "<option>11日</option>"+
            "<option>12日</option>"+
            "<option>13日</option>"+
            "<option>14日</option>"+
            "<option>15日</option>"+
            "<option>16日</option>"+
            "<option>17日</option>"+
            "<option>18日</option>"+
            "<option>19日</option>"+
            "<option>20日</option>"+
            "<option>21日</option>"+
            "<option>22日</option>"+
            "<option>23日</option>"+
            "<option>24日</option>"+
            "<option>25日</option>"+
            "<option>26日</option>"+
            "<option>27日</option>"+
            "<option>28日</option>";
        var day = $("#day");
        var strMont = $("#month").val();
        if(strMont != "2月"){
            if(strMont =="1月"|strMont =="3月"||strMont =="5月"||strMont =="7月"||strMont =="8月"||strMont =="10月"||strMont =="12月"){
                base = base + "<option>29日</option><option>30日</option><option>31日</option>";
            } else {
                base = base +  "<option>29日</option><option>30日</option>";
            }
        } else {
            // 判断是否为闰年
            var strYear = $("#year").val();
            var iYear = parseInt(strYear);
            if(((iYear%4==0)&&(iYear%100!=0))||(iYear%400==0)) {
                base = base + "<option>29日</option>";
            }
        }
        $("#day").html(base);
    });
}